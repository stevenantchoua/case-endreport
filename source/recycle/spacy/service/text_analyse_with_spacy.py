import spacy

class SpacyTextReportAnalyeService:
    
    def __init__(self,path):
        self.nlp = spacy.load("en_core_web_sm")
        with open(path,"r") as f:
            self.text = f.read()

        self.doc = self.nlp(self.text)

    def getSentenceWithScope(self):
        listSentenceWithScope = {}
        previousLine = ""
        key = "Scope "
        keySmall = "scope "

        for sent in self.doc.sents:
            for i in range(1, 4):
                if sent.text.find(key + str(i)) > 0 or sent.text.find(keySmall + str(i)) > 0:
                    listSentenceWithScope[previousLine] = sent
            previousLine = sent

        return listSentenceWithScope

    def getSentenceOnlyWithNumber(self):
        resultat = []
        for sent in self.doc.sents:
            for token in sent:
                if token.is_digit > 0 and sent.text.find('metadata') < 0 :
                    resultat.append(sent)
        return resultat

    def saveSentenceWithScopeOnDB(data):
        #TODO add code to save data on postgresql
        return ""

#Test
spacyTextReportAnalye = SpacyTextReportAnalyeService("source/spacy/ressource/daimler.txt")
print(spacyTextReportAnalye.getSentenceWithScope())