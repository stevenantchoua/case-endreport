import sqlite3
import uuid

class DatabaseVerweis:
    def __init__(self):
        DatabaseVerweis.init_db(self)

    def init_db(self):
        con = sqlite3.connect("report_result_analyse.db")
        
        cur = con.cursor()
        
        cur.execute("""
        CREATE TABLE  IF NOT EXISTS t_results (
            result_id BIGINT UNSIGNED NOT NULL,
            p_scope TEXT NOT NULL,
            p_page_number TEXT NOT NULL,
            p_text TEXT NOT NULL)
        """)
        print("db well init")
        con.close
    
    def save(self, scope,verweis,verweisdata):
        con = sqlite3.connect("report_result_analyse.db")
        
        cur = con.cursor()
        cur.execute("INSERT INTO t_results VALUES (?,?,?,?)", ((str(uuid.uuid4().fields[-1])[:5]),scope,verweis,verweisdata))
        con.commit()
        con.close
