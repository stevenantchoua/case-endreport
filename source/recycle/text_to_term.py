"""
@author: Leon Geis, Steve Nantchoua
This file can be used to extract and print
the scopes and the page in which they are
mentioned. They are extracted as a nested dictionary
and printed to console.

Datastructure:
    Dictionary:
                Scope 1: [Page_1, Content_1], ..., [Page_n, Content_n]
                Scope 2: [Page_1, Content_1], ..., [Page_n, Content_n]
                Scope 3: [Page_1, Content_1], ..., [Page_n, Content_n]

Here Scope 1, Scope 2 and Scope 3 are also dictionaries.
"""

#!/usr/bin/python3

import argparse
from pathlib import Path

from database_verweis import DatabaseVerweis

class ManualTextReportAnalyeService:
    def __init__(self,path):
        self.path = path
        self.databaseVerweis =  DatabaseVerweis()

    def data_provider(self):
        with open(self.path, "r") as f:
            return f.read().splitlines()

    def __get_text_content_for_given_keyword(self, keyword):
        txt_data = ManualTextReportAnalyeService.data_provider(self)
        previous_line = ""
        keyword_dict = {}
        for line in txt_data:
            if keyword in line:
                keyword_dict[previous_line] = line
            previous_line = line
        return keyword_dict


    def create_scope_dictionary(self):
        scope_dictionary = {}
        for i in range(1, 4):
            keyword = "Scope " + str(i)
            keyword_small = "scope " + str(i)

            scope_dictionary[keyword] = ManualTextReportAnalyeService.__get_text_content_for_given_keyword(self,keyword)
            scope_dictionary[keyword].update(ManualTextReportAnalyeService.__get_text_content_for_given_keyword(self,keyword_small))
        return scope_dictionary

    
    def save_scope_dictionary(self):
        dictionary = ManualTextReportAnalyeService.create_scope_dictionary(self)
        for scope in dictionary: 
            for verweis in dictionary[scope]:
                self.databaseVerweis.save(scope,verweis,dictionary[scope][verweis])

def main():
    # Line below used for testing purposes
    manualTextReportAnalyeService = ManualTextReportAnalyeService("ressource/daimler.txt")

    manualTextReportAnalyeService.save_scope_dictionary()


if __name__ == "__main__":
    main()
