#!/usr/bin/python3
import argparse
from pathlib import Path

"""
@author: Leon Geis
This file uses the .txt files created by 
pdf_parsing.py to extract out 
of each page, those sentences
which contain a numeric value.

Datastructure:

    list = [s_1, ..., s_n]
    
Note: Each s_1 to s_n is a sentence which
contains a numeric value.
"""


def get_numeric_sentences(path, include_pages=False):
    with open(path, "r") as f:
        # Skip Metadata part at the beginning
        txt_data = f.read().splitlines()[8:]
        numeric_sentences = []
        for line in txt_data:
            if "PDF page:" not in line:
                # Split the String of the page
                sentences = line.split(". ")
                # Iterate over the sentences
                for sentence in sentences:
                    # Check if sentence contains a numeric value
                    if any(str.isdigit(char) for char in sentence):
                        numeric_sentences.append(sentence)
    return numeric_sentences


def print_numeric_sentences(path):
    ns = get_numeric_sentences(path)
    # Iterate over each numeric sentence
    for n in ns:
        print(n+"\n")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file_path", type=Path)
    arg = parser.parse_args()
    print_numeric_sentences(arg.file_path)


if __name__ == "__main__":
    main()
