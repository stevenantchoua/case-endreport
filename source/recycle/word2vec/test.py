#!/usr/bin/python3
import os
from pathlib import Path

import nltk
import spacy
from anytree import RenderTree
from spacy import displacy
import numeric_sentence_extractor
import gensim
from nltk import Tree
import warnings

from source import pdf_parsing, text_to_term
from source.word2vec import skip_gram_model_trainer

warnings.filterwarnings(action='ignore')


# requirements:
#   python3 -m spacy download en_core_web_sm


def main():
    #nlp = spacy.load("en_core_web_sm")
    # Pass the path of the parsed .txt file
    scope_dictionary = text_to_term.create_scope_dictionary("/home/leon/PycharmProjects/pj-intelligente-system"
                                                            "-finazmarkt/source/word2vec/outputbasf.txt")

    result = skip_gram_model_trainer.compute_word_page_references(scope_dictionary, "Scope", 4, 6, 0.9)
    print(result)


if __name__ == "__main__":
    main()
