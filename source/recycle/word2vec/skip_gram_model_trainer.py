#!/usr/bin/python3
import gensim
import spacy
from anytree import Node, RenderTree

"""
@author: Leon Geis
This module can be easily used to
create a trained Word2Vec Skip-Gram model
and get the similar words out of a given word
w.r.t the given (trained) model.
"""


def train(token_list, vector_size, min_count, window):
    """
    Creates a trained Skip-Gram Word2Vec Model.
    :param token_list: List of tokens the model is trained on
    :param vector_size: The size of the vector for each word
    :param min_count: The amount of occurrences a word has w.r.t
                      a token_list to be used for computation
    :param window: The window size for each word
    :return: The trained Skip-Gram model.
    """
    sg_model = gensim.models.Word2Vec(token_list, vector_size=vector_size, min_count=min_count, window=window, sg=1)
    return sg_model


def get_vector(token, trained_model):
    """
    Get the vector of a token (or word) w.r.t.
    the given trained model.
    :param token: Word which vector shall be returned
    :param trained_model: The trained Word2Vec model
    :return: Vector representation of the given word.
    """
    return trained_model.wv[token]


def get_vectors(tokens, trained_model):
    """
    Get a List of Vectors for each token w.r.t.
    the given trained Word2Vec model.
    :param tokens: List of tokens (or words)
    :param trained_model: The trained Word2Vec model
    :return: A List consisting of vector representations for each token
             given in the tokens list.
    """
    vectors = []
    for token in tokens:
        vectors.append(trained_model.wv[token])
    return vectors


def get_similarity(word, trained_model, size = 10):
    """
    :param word: The word which similarities (other words)
                 are to be computed
    :param trained_model: The trained Word2Vec Model
    :param size: The size of the list to be returned (default = 10)
    :return: A list of similar words as tuples in the vocabulary.
             The tuples have the form (similar word, [0,1]
             similarity measure).
             If the word is not in the vocabulary of the trained model
             the similar words of the word with all lowercase characters
             will be returned instead. Otherwise the word is not present
             the vocabulary.
             (Note: The vocabulary is accessible with key_to_index)
    """
    if word in trained_model.wv.key_to_index:
        return trained_model.wv.most_similar(trained_model.wv[word], topn=size)
    else:
        return trained_model.wv.most_similar(trained_model.wv[word.lower()], topn=size)


def compute_similarity_tree(word, trained_model, depth, max_size=10, similarity_measure=0.0):
    """
    Computes the similarity tree of the given word.
    :param word:
    :param trained_model:
    :param depth:
    :param max_size:
    :param similarity_measure:
    :return:
    """
    # Create Node list
    node_list = [Node(word)]

    # Create node counter
    node_counter = 0

    # Create similarity tree of depth = depth
    for i in range(0, depth):
        # Create amount of new nodes in current layer
        amount_nodes = len(node_list)-node_counter

        for amount_node in range(0, amount_nodes):

            # Get node at position node_counter in node_list
            current_node = node_list[node_counter]

            # Get similar words w.r.t to the given word (Expand Node)
            similar_word_list = get_similarity(current_node.name, trained_model, max_size)

            # Iterate through similar word list
            for similar_word in similar_word_list:
                # Check for similarity measure
                # and do not add the same word to the tree
                if similar_word[1] >= similarity_measure and similar_word[0] != current_node.name:
                    # Add the word to the list as a node
                    node_list.append(Node(similar_word[0], parent=current_node))
            # Increment Node Counter
            #print(node_counter)
            #print(node_list)
            node_counter += 1

    # Return node list
    return node_list


def print_similarity_tree(word, trained_model, depth, max_size=10, similarity_measure=0.0):
    root = compute_similarity_tree(word, trained_model, depth, max_size, similarity_measure)[0]
    for pre, fill, node in RenderTree(root):
        print("%s%s" % (pre, node.name))


def get_similar_values_from_tree(word, similarity_tree):
    similar_values = []
    word_found = False
    for node in similarity_tree:
        # To guarantee that the words are equal
        # if they contain the same lower case
        # characters
        if node.name.lower() == word.lower():
            word_found = True

    if word_found:
        for node in similarity_tree:
            if any(str.isdigit(char) for char in node.name):
                similar_values.append(node.name)

    return similar_values


def get_similar_values_from_multiple_trees(word, similarity_trees):
    similar_values = []
    for tree in similarity_trees:
        tree_values = get_similar_values_from_tree(word, tree)
        for value in tree_values:
            if value not in similar_values:
                similar_values.append(value)
    return similar_values


def compute_word_page_references(scope_dictionary, word,depth, max_size, similarity_measure):
    # Access Spacy Pipeline
    nlp = spacy.load("en_core_web_sm")
    # Create empty token list
    token_list = []
    # Create empty list for numbers
    nums = []
    for scope in scope_dictionary:
        for page in scope_dictionary[scope]:
            doc = nlp(scope_dictionary[scope][page])
            temp = [page]
            for token in doc:
                # To even consider falsely parsed tokens
                # the if condition below is necessary
                # and not [if token.like_num ...]
                if any(str.isdigit(char) for char in token.text):
                    nums.append(token)
                temp.append(token.text)
            token_list.append(temp)
    # Create a trained Word2Vec Skip-Gram model
    model = train(token_list,100,1,15)

    # Get all similar words
    similar_words = get_similarity(word,model, 5)

    # Iterate over all similar words and store the longest list
    # which is then returned
    most_semantic_references = []
    for similar_word in similar_words:
        # Create all similarity tree w.r.t the Scope
        # and the numbers in the computed token_list (nums)
        trees = []
        for token in nums:
            # print_similarity_tree(token.text,model,depth,max_size=max_size,similarity_measure=similarity_measure)
            trees.append(compute_similarity_tree(token.text, model, depth, max_size=max_size, similarity_measure=similarity_measure))

        # Get all similar values w.r.t. the given word
        scope_similar_values = get_similar_values_from_multiple_trees(similar_word[0], trees)

        # Create dictionary to be returned
        page_references = []

        # Add the corresponding page to the value
        for p in scope_similar_values:
            for page_and_tokens in token_list:
                # Check if page is already contained in
                # page_references
                if p in page_and_tokens:
                    # Iterate over page_references list
                    check = False
                    for reference in page_references:
                        if page_and_tokens[0] in reference[0]:
                            if p not in reference:
                                reference.append(p)
                            check = True

                    # Get the according key (page)
                    # and add the value
                    if not check:
                        page_references.append([page_and_tokens[0],p])

        # Iterate over page_references list
        # and check if a longer list is contained
        for page_reference in page_references:

            if len(most_semantic_references) < len(page_reference):
                most_semantic_references = page_reference


    # Return the list
    return most_semantic_references
