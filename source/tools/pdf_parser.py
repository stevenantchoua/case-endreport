#!/usr/bin/python3

from database.DatabaseReference import DatabaseReference

import argparse
from datetime import datetime
import faulthandler
import hashlib
import os
import sys
import logging, logging.handlers
from typing import Optional

import pdftotext

from pdfreader.types.native import String
from pdfreader import PDFDocument


# get detailed information in case of a fault
faulthandler.enable()


# setup logging
log_format = '%(asctime)s - %(levelname)s: %(message)s'
logging.basicConfig(stream=sys.stdout, format=log_format, level=logging.INFO)
log = logging.getLogger()

log_file_handler = logging.handlers.RotatingFileHandler("pdf_parser.log")
log_file_handler.setFormatter(logging.Formatter(log_format))
log.addHandler(log_file_handler)


# will be initialized by argparse
program_args = {}


def parse_metadata(pdf_path) -> Optional[datetime]:
    try:
        with open(pdf_path, "rb") as f:
            pdf = PDFDocument(f)
            # FIXME: The metadate extracted from pdf_reader is not reliable or may not set in the documents correctly, therefore for a lot documents manual work is necessary.
            creationDate = pdf.metadata['CreationDate']
            if isinstance(creationDate, datetime):
                return creationDate
            elif isinstance(creationDate, str):
                pass
            elif isinstance(creationDate, String):
                pass
    except Exception:
        pass
        # log.exception("parse_metadata() failed.")
    return None


def pdf_to_list_of_pages_PDFTOTEXT(pdf_path) -> Optional[list]:
    try:
        with open(pdf_path, "rb") as f:
            pdf = pdftotext.PDF(f)
            pages = []
            for page in pdf:
                pages.append(str(page))
            return pages
    except Exception:
        log.exception("pdf_to_list_of_pages_PDFTOTEXT() failed.")
    return None


def parse_pdf(pdf_path) -> Optional[dict]:
    try:
        log.info(f"Begin with '{pdf_path}'")
        pdf_dict = {}
        pdf_dict['basename'] = os.path.basename(pdf_path)
        pdf_dict['realease_date'] = parse_metadata(pdf_path)
        pdf_dict['pages'] = pdf_to_list_of_pages_PDFTOTEXT(pdf_path)
        return pdf_dict
    except Exception:
        log.exception(f"Error while parsing '{pdf_path}' occured.")
    return None


def write_document_to_txt(pdf_dict:dict):
    try:
        with open(str(pdf_dict['basename'])[:-4] + ".txt", 'w') as txt:
            for index, page in enumerate(pdf_dict['pages']):
                txt.write('--- index: ' + str(index) + ' ---\n')
                txt.write(page)
                txt.write('\n')
            log.info("write_document_to_txt() succeed.")
    except Exception:
        log.exception("Error while saveing pdf results. ")


def write_document_to_db(pdf_dict:dict):
    try:
        name = pdf_dict['basename']
        pages = pdf_dict['pages']
        # FIXME: assumption -> in context of this project all docs are in english
        language = pdf_dict.get('language')
        if language == None:
            language = "en"

        release_date = pdf_dict.get('realease_date')

        global program_args
        check_checksum = not program_args.no_check
        checksum = ""
        if check_checksum:
            # Callculate checksum
            sha256 = hashlib.sha256()
            for page in pages:
                sha256.update(str(page).encode())
            checksum = sha256.hexdigest()

        doc_id = DatabaseReference.write_document_to_database(name=name, language=language, release_date=release_date, pages=pages, checksum=checksum, check_checksum=check_checksum)
        if doc_id != None:
            log.info(f"Doc-ID: {doc_id} created.")
        else:
            log.error("write_document_to_db() failed.")
    except Exception:
        log.exception("Error while saveing pdf results. ")


def save_results(pdf_dict:dict):
    global program_args
    if program_args.txt:
        write_document_to_txt(pdf_dict)
    else:
        write_document_to_db(pdf_dict)



def main():
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('path', help="File or directory name. Only *.pdf filenames will be parsed.")
    parser.add_argument('--txt', dest="txt", action='store_true', help="Redirects result into local *.txt file.")
    parser.add_argument('--no_check', dest="no_check", action='store_true', help="Disables checksum check against already inserted documents.")
    global program_args
    program_args = parser.parse_args()

    # program start
    if os.path.isfile(program_args.path) and str(program_args.path).endswith('.pdf'):
        save_results(parse_pdf(program_args.path))

    elif os.path.isdir(program_args.path):
        for entry in os.listdir(program_args.path):
            entry_path = f"{program_args.path}/{entry}"
            if os.path.isfile(entry_path) and entry_path.endswith('.pdf'):
                save_results(parse_pdf(entry_path))

    else:
        log.error(f"Provided path is not directory, file or file which ends with '*.pdf'. Path: '{program_args.path}' ")


if __name__ == "__main__":
    main()
