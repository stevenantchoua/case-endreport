#!/usr/bin/python3


# TODO: Not completed
exit()


from database.DatabaseReference import DatabaseReference
import spacy

from spacy.lang.en import English
from spacy.matcher import PhraseMatcher
import argparse
import json
import re




# scope
def find_scopes_on_page(page_doc):
    matches = []
    for token in page_doc:
        if token.lower_.startswith("scope"):
            next_token = page_doc[token.i + 1]
            if next_token.like_num:
                if 1 <= int(next_token.text) <= 3:
                    matches.append(str(page_doc[token.i:token.i + 2]))
    return matches


def find_scopes(doc_page_list):
    nlp = spacy.load('en_core_web_sm') 
    matches = {}
    matches['scope_matches'] = {index: find_scopes_on_page(nlp(page)) for index, page in enumerate(doc_page_list) if len(find_scopes_on_page(nlp(page))) != 0}

    


    regex = r"[Ss]cope"
    for index ,page in enumerate(doc_page_list):
        doc = nlp(page)



        matcher = PhraseMatcher(nlp.vocab)
        keywords = ['scope', 'Scope']
        pattern = [nlp(page) for page in keywords]
        matcher.add('scope', None, *pattern)

        doc = nlp(page)

        for sent in doc.sents:
            for match_id, start, end in matcher(nlp(sent.text)):
                if nlp.vocab.strings[match_id] in ["scope"]:
                    print(f"Page: {index}")
                    print(sent.text)

        # for match in re.finditer(regex, doc.text):
        #     beg, end = match.span()
        #     match = doc.char_span(beg, end)
        #     print(type(match))


    return matches


# co2
def find_co2_on_page(page_doc):
    matches = []
    for token in page_doc:
        if token.lower_ == "co2":
            matches.append(str(token))
            continue

        if token.lower_ == "co":
            next_token = page_doc[token.i + 1]
            if next_token.like_num:
                if int(next_token.text) == 2:
                    matches.append(str(page_doc[token.i:token.i + 2]))
    return matches


def find_co2(doc_page_list):
    nlp = English()
    matches = {}
    matches['co2_matches'] = {index: find_co2_on_page(nlp(page)) for index, page in enumerate(doc_page_list) if len(find_co2_on_page(nlp(page))) != 0}
    return matches



def main():
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('doc_id', help="Document ID")
    program_args = parser.parse_args()

    doc_page_list = DatabaseReference.query_doc(1) # program_args.doc_id)

    results = {}
    results["doc_id"] = program_args.doc_id

    # scopes
    results['scope_matches'] = find_scopes(doc_page_list['pages'])

    # co2
    results['co2_matches'] = find_co2(doc_page_list)




    print(json.dumps(results, indent=2))
    # DatabaseReference.store_scope_results_db(results)


if __name__ == "__main__":
    main()
