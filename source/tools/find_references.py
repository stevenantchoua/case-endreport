#!/usr/bin/python3

from database.DatabaseReference import DatabaseReference

from spacy.lang.en import English
import argparse
import logging
import sys


# setup logging
log_format = '%(asctime)s - %(levelname)s: %(message)s'
logging.basicConfig(stream=sys.stdout, format=log_format, level=logging.INFO)
log = logging.getLogger()


# FIXME: create nlp object based on language attribute received from database
nlp = English()


# keywords
ghg = ['co2', 'ch4', 'n2o', 'hfc', 'pfc', 'sf6']


# scope
def find_scopes_on_page(page_doc):
    matches = set()
    for token in page_doc:
        if token.lower_.startswith("scope"):
            next_token = page_doc[token.i + 1]
            if next_token.like_num and 1 <= int(next_token.text) <= 3:
                matches.add(str(page_doc[token.i:token.i + 2]))
    return list(matches)


#ghg
def find_match_on_page(page_doc, key) -> list:
    for token in page_doc:
        if token.lower_.startswith(key):
            return True


def find_references(doc_page_list) -> dict:
    global nlp, ghg

    # init
    matches = {}
    matches['scope'] = {}
    for key in ghg:
        matches[key] = []

    # find page references
    # NOTE: Index starting from 0 and pdf normally from 1 therefore: index + 1 = actual pdf_page
    for index, page in enumerate(doc_page_list):
        page_doc = nlp(page)

        page_matches = find_scopes_on_page(page_doc)
        if len(page_matches) != 0:
            matches['scope'][index] = page_matches

        for key in ghg:
            if find_match_on_page(page_doc, key):
                matches[key].append(index)
    return matches


def main():
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('doc_id', help="Document ID")
    parser.add_argument('--save', dest="save", action='store_true', help="Saves results to t_references")
    program_args = parser.parse_args()

    doc_page_list = DatabaseReference.query_doc(program_args.doc_id)
    if doc_page_list != None:
        results = {}
        results["doc_id"] = program_args.doc_id
        results["references"] = find_references(doc_page_list['pages'])

        if program_args.save:
            DatabaseReference.write_find_reference_results(results)
        print(results)
    else:
        log.info(f"Document ID {program_args.doc_id} not found. ")

if __name__ == "__main__":
    main()

