#!/usr/bin/python3

from mysql import connector as mdb
from datetime import datetime
from typing import Optional
import logging
import json
import faulthandler

# get detailed information in case of a fault
faulthandler.enable()


DB_CONNECTION_SETTINGS = {
    "host": "localhost",
    "port": "3306",
    "database": "project",
    "user": "admin",
    "password": "ADMINPASSWORD"
}


class DatabaseReference():
    def query_doc(id) -> Optional[dict]:
        try:
            conn = mdb.connect(**DB_CONNECTION_SETTINGS)
            cur = conn.cursor()

            select_query = "SELECT name,language,release_date,checksum,insertion_date FROM t_documents WHERE doc_id=%(doc_id)s LIMIT 1"
            cur.execute(select_query, {"doc_id": id})
            res = cur.fetchone()

            doc = {}
            doc['name'] = res[0]
            doc['language'] = res[1]
            doc['release_date'] = str(res[2])
            doc['checksum'] = res[3]

            select_query = "SELECT page_number,page_text FROM t_pages WHERE doc_id=%(doc_id)s"
            cur.execute(select_query, {"doc_id": id})
            pages = []
            for res in cur:
                pages.append(res[1])
            doc['pages'] = pages

            return doc
        except Exception:
            logging.exception("query_doc() failed.")
        return None


    def query_doc_references(id) -> Optional[list]:
        try:
            conn = mdb.connect(**DB_CONNECTION_SETTINGS)
            cur = conn.cursor()

            select_query = "SELECT id,insertion_date,doc_id,results FROM t_references WHERE doc_id=%(doc_id)s"
            cur.execute(select_query, {"doc_id": id})
            result = []
            for res in cur:
                temp = {}
                temp['id'] = res[0]
                temp['insertion_date'] = str(res[1])
                temp['doc_id'] = res[2]
                temp['results'] = res[3]
                result.append(temp)
            return result
        except Exception:
            logging.exception("query_doc() failed.")
        return None


    def write_document_to_database(name="", language=None, release_date=None, pages=[], checksum="", check_checksum=True) -> Optional[int]:
        try:
            with mdb.connect(**DB_CONNECTION_SETTINGS) as dbcon:
                dbcon.autocommit = False
                dbcur = dbcon.cursor()

                if release_date != None:
                    if isinstance(release_date, datetime):
                        release_date = release_date.strftime('%Y-%m-%d %H:%M:%S')
                    else:
                        raise TypeError("write_document_to_database() failed. 'release_date' must be instance of datetime.dateime. ")


                # check if document already exist
                if check_checksum:
                    select_query = "SELECT doc_id,checksum FROM t_documents"
                    dbcur.execute(select_query)
                    for entry in dbcur.fetchall():
                        if str(entry[1]) == str(checksum):
                            logging.warn(f"Document with ID '{str(entry[0])}' has the same checksum. Skip insertion.")
                            return None

                # create document entry
                result = dbcur.callproc('p_create_doc', [name, language, release_date, checksum, 0])
                doc_id = result[4]
                if doc_id == None:
                    logging.error("Cloud not create document entry in table 't_documents'.")
                    dbcon.rollback()
                    return None

                # insert documents page
                insert_page = ("INSERT INTO t_pages (doc_id, page_number, page_text)"
                                    "VALUES (%(doc_id)s, %(page_number)s, %(page_text)s)")

                if pages  != None:
                    page_num = 0
                    for page in pages:
                        dbcur.execute(insert_page, {"doc_id": doc_id, "page_number": page_num, "page_text": page})
                        page_num += 1

                dbcon.commit()
                return doc_id
        except Exception:
            logging.exception("write_document_to_database() failed")
        return None


    def write_find_reference_results(results):
        try:
            conn = mdb.connect(**DB_CONNECTION_SETTINGS)
            cur = conn.cursor()

            insert_scope_reference = ("INSERT INTO t_references (doc_id, results)"
                                        "VALUES (%(doc_id)s, %(results)s)")

            cur.execute(insert_scope_reference, {"doc_id": results['doc_id'], "results": json.dumps(results['references'])})
            conn.commit()
        except Exception:
            logging.exception("write_document_to_database() failed")
