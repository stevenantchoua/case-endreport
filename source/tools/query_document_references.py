#!/usr/bin/python3

from database.DatabaseReference import DatabaseReference

import argparse
import json
import sys


def main():
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('doc_id', help="Document ID")
    program_args = parser.parse_args()

    doc_page_list = DatabaseReference.query_doc_references(program_args.doc_id)
    print(json.dumps(doc_page_list, indent=2))


if __name__ == "__main__":
    main()
