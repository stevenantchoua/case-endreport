#!/usr/bin/python3

import pytesseract as ocr
import argparse
from pathlib import Path
import json

class OcrImageAnalyseService:
    def __init__(self,path):
        self.textFromPicture = ocr.image_to_string(str(path))
    
    def extractScope1UpTo3FromPicture(self):     
        resultat = []   
        for i in range(3):
            searchWordBig = "Scope "+ str((i+1))
            if self.textFromPicture.find(searchWordBig) > 0:
                extractedTextForScope = []
                indexFoundText = self.textFromPicture.find(searchWordBig)
                
                if "Scope 1" in searchWordBig:
                    limitIndexToExtractText = self.textFromPicture.find("%",indexFoundText)-1
                    extractedTextForScopeTemp = self.textFromPicture[indexFoundText:limitIndexToExtractText]
                    extractedTextForScope = extractedTextForScopeTemp.split()
                else:
                    extractedTextForScopeTemp = self.textFromPicture[indexFoundText:self.textFromPicture.find("\n%",indexFoundText)-1]
            
                    for text in extractedTextForScopeTemp.split():
                        list = len(text.split("%"))
                        if(list >1):
                            break
                        else:
                            extractedTextForScope.append(text)
                
                extractedTextForScope.insert(0,extractedTextForScope[0] + " "+ extractedTextForScope[1])
                OcrImageAnalyseService.__removeRedundantInformationOnList(self,extractedTextForScope)
                resultat.append(extractedTextForScope)
        return resultat
        
    def __removeRedundantInformationOnList(self,extractedTextForScopeList):
        extractedTextForScopeList.pop(1)
        extractedTextForScopeList.pop(1)  

def main():        
    parser = argparse.ArgumentParser()
    parser.add_argument("file_path", type=Path)
    arg = parser.parse_args()

    ocrImageAnalyseService = OcrImageAnalyseService(arg.file_path)
    json_string = json.dumps(ocrImageAnalyseService.extractScope1UpTo3FromPicture(), indent=2)
    
    print(json_string)

if __name__ == "__main__":
    main()
