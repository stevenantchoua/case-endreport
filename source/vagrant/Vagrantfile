# -*- mode: ruby -*-
# vi: set ft=ruby :

nodes = {
  "arch": {
    box: "archlinux/archlinux",
    box_version: "=20211201.40458",
    fwd_port: ["3306", "3000"],
    network: ["172.10.1.100"],
    playbook: [
      "./machines/arch/base.yml",
      "./machines/arch/mysql.yml",
      "./machines/arch/grafana.yml",
      ]
  }
}


Vagrant.configure("2") do |config|
  nodes.each do |name, node|
    config.vm.define "#{name}" do |vm|
      vm.vm.provider "libvirt" do |p|
        p.memory = "2048"
        p.cpus = "2"
      end

      vm.vm.provider "virtualbox" do |p|
        p.gui = false
        p.memory = "2048"
        p.cpus = "2"
        # p.customize ["modifyvm", :id, "--nested-hw-virt", "on"]
      end

      vm.vm.hostname = "#{name}"
      vm.vm.box = "#{node[:box]}"
      vm.vm.box_version = "#{node[:box_version]}"
      vm.vm.synced_folder '.', '/vagrant', disabled: true


      # private networks
      if node[:network].nil? == false
        node[:network].each do |ip|
          vm.vm.network "private_network", ip: ip
        end
      end

      # forwarded ports
      if node[:fwd_port].nil? == false
        node[:fwd_port].each do |port|
          vm.vm.network "forwarded_port", guest: "#{port}", host: "#{port}"
        end
      end

      # ansible
      if node[:playbook].nil? == false
        node[:playbook].each do |playbook|
          vm.vm.provision "ansible" do |ansible|
            ansible.playbook = "#{playbook}"
            ansible.extra_vars = {ansible_python_interpreter:"/usr/bin/python3"}
          end
        end
      end
    end
  end
end
