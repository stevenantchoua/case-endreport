#!/bin/bash
pushd "$(dirname "$0")"

rm ./ca-cert.pem
rm ./ca-key.pem
rm ./grafana-key.pem
rm ./grafana-cert.pem
rm ./grafana-req.pem

openssl genrsa 4096 > ./ca-key.pem
yes "" | openssl req -x509 -config ./cnf/ca.cnf -key ca-key.pem -out ca-cert.pem
yes "" | openssl req -config cnf/grafana.cnf -newkey rsa:4096 -sha1 -nodes -out grafana-req.pem
openssl rsa -in ./grafana-key.pem -out ./grafana-key.pem
openssl x509 -req -in grafana-req.pem -CA ca-cert.pem  -CAkey ca-key.pem -set_serial 01 -out grafana-cert.pem

rm ./grafana-req.pem
