#!/bin/bash
pushd "$(dirname "$0")"

rm ./ca-cert.pem
rm ./ca-key.pem
rm ./mariadb-key.pem
rm ./mariadb-cert.pem
rm ./mariadb-req.pem

openssl genrsa 4096 > ./ca-key.pem
yes "" | openssl req -x509 -config ./cnf/ca.cnf -key ca-key.pem -out ca-cert.pem
yes "" | openssl req -config cnf/mariadb.cnf -newkey rsa:4096 -sha1 -nodes -out mariadb-req.pem
openssl rsa -in ./mariadb-key.pem -out ./mariadb-key.pem
openssl x509 -req -in mariadb-req.pem -CA ca-cert.pem  -CAkey ca-key.pem -set_serial 01 -out mariadb-cert.pem

rm ./mariadb-req.pem
