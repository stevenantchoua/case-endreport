-- ### database ###
CREATE DATABASE IF NOT EXISTS project
    CHARACTER SET='utf8mb4'
    COLLATE='utf8mb4_unicode_ci';



-- ### tables ###
USE project;

CREATE TABLE IF NOT EXISTS t_documents (
    doc_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(4096) NOT NULL,
    language VARCHAR(512) DEFAULT(NULL),
    release_date DATETIME DEFAULT(NULL),
    checksum VARCHAR(512) NOT NULL,
    insertion_date DATETIME NOT NULL DEFAULT(NOW()),
    PRIMARY KEY(doc_id),
    KEY i_name (name)
) ENGINE='InnoDB';


CREATE TABLE IF NOT EXISTS t_pages (
    page_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    doc_id BIGINT UNSIGNED NOT NULL, FOREIGN KEY(doc_id) REFERENCES t_documents(doc_id) ON DELETE CASCADE,
    page_number BIGINT UNSIGNED NOT NULL,
    page_text MEDIUMTEXT NOT NULL,
    PRIMARY KEY(page_id),
    KEY i_page_number (page_number)
) ENGINE='InnoDB';


CREATE TABLE IF NOT EXISTS t_references (
    id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    doc_id BIGINT UNSIGNED NOT NULL, FOREIGN KEY(doc_id) REFERENCES t_documents(doc_id) ON DELETE CASCADE,
    results JSON NOT NULL,
    insertion_date DATETIME NOT NULL DEFAULT(NOW()),
    PRIMARY KEY(id)
) ENGINE='InnoDB';


-- CREATE TABLE IF NOT EXISTS t_results (
--     result_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
--     doc_id BIGINT UNSIGNED NOT NULL, FOREIGN KEY(doc_id) REFERENCES t_documents(doc_id) ON DELETE CASCADE,
--     result_text MEDIUMTEXT NOT NULL,
--     PRIMARY KEY(result_id),
--     KEY i_page_number (page_number)
-- ) ENGINE='InnoDB';


-- ### procedure ###
DELIMITER //
CREATE PROCEDURE IF NOT EXISTS p_create_doc
(
    IN in_name VARCHAR(4096),
    IN in_language VARCHAR(512),
    IN in_release_date DATETIME,
    IN in_checksum VARCHAR(512),
    OUT out_doc_id BIGINT UNSIGNED
)
proc:BEGIN
    INSERT INTO t_documents(name, language, release_date, checksum) VALUES(in_name, in_language, in_release_date, in_checksum);
    SET out_doc_id = (SELECT LAST_INSERT_ID());
END //
DELIMITER ;




-- ### users ###
DROP USER IF EXISTS 'admin'@'localhost';
CREATE USER IF NOT EXISTS 'admin'@'localhost' IDENTIFIED BY 'ADMINPASSWORD' REQUIRE SSL;
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'localhost';

DROP USER IF EXISTS 'admin'@'arch';
CREATE USER IF NOT EXISTS 'admin'@'arch' IDENTIFIED BY 'ADMINPASSWORD' REQUIRE SSL;
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'arch';

DROP USER IF EXISTS 'admin'@'%';
CREATE USER IF NOT EXISTS 'admin'@'%' IDENTIFIED BY 'ADMINPASSWORD' REQUIRE SSL;
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%';


DROP USER IF EXISTS 'dashboard'@'arch';
CREATE USER IF NOT EXISTS 'dashboard'@'arch' IDENTIFIED BY 'DASHBOARDPASSWORD' REQUIRE SSL;
GRANT SELECT ON project.* TO 'dashboard'@'arch';

DROP USER IF EXISTS 'dashboard'@'localhost';
CREATE USER IF NOT EXISTS 'dashboard'@'localhost' IDENTIFIED BY 'DASHBOARDPASSWORD';
GRANT SELECT ON project.* TO 'dashboard'@'localhost';

DROP USER IF EXISTS 'dashboard'@'%';
CREATE USER IF NOT EXISTS 'dashboard'@'%' IDENTIFIED BY 'DASHBOARDPASSWORD' REQUIRE SSL;
GRANT SELECT ON project.* TO 'dashboard'@'%';

FLUSH PRIVILEGES;
